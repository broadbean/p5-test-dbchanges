package Test::DBChanges::Role::DBI;
use Moo::Role;
use Types::Standard qw(HasMethods);
use namespace::autoclean;

# VERSION
# ABSTRACT: adapt DBChanges to DBI

=head1 DESCRIPTION

Classes that talk directly to DBI should consume this role. It
provides a C<dbh> attribute, whose value should be a database handle
as returned by C<< DBI->connect >>.

=attr C<dbh>

Required, the database handle to track changes on.

=cut

has dbh => ( is => 'ro', required => 1, isa => HasMethods[qw(do selectall_arrayref)] );

sub _db_do {
    my ($self,$sql,@args) = @_;
    my $dbh = $self->dbh;
    # silence "NOTICE: the relation already exists"
    local $dbh->{PrintWarn} = 0;
    $dbh->do($sql,{},@args);
}

sub _db_fetch {
    my ($self,$sql,@args) = @_;
    return $self->dbh->selectall_arrayref($sql, { Slice => {} }, @args);
}

sub _table_and_factory_for_source {
    my ($self,$source_name) = @_;

    # source names are table names, and we don't transform the result
    # of _db_fetch
    return ( $source_name, sub { return @_ } );
}

1;
