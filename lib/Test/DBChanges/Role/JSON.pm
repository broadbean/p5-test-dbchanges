package Test::DBChanges::Role::JSON;
use Moo::Role;
use JSON::MaybeXS ();
use namespace::autoclean;

# VERSION
# ABSTRACT: decode data that's recorded as JSON

=head1 DESCRIPTION

Classes that store changes as JSON should consume this role.

=for Pod::Coverage decode_recorded_data

=cut

sub decode_recorded_data {
    my ($self, $recoded_data) = @_;

    return JSON::MaybeXS::decode_json($recoded_data);
}

1;
